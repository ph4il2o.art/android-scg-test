package com.art.scgtest

import java.lang.Exception

sealed class UseCaseResult<out T : Any> {
    class Success<out T : Any>(val data: T) : UseCaseResult<T>()
    class Error(val exception: Exception) : UseCaseResult<Nothing>()
}