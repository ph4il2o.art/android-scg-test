package com.art.scgtest.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.art.scgtest.R
import com.art.scgtest.adapter.ProductItemAdapter
import com.art.scgtest.model.data.Stock
import com.art.scgtest.viewmodel.MachineViewModel
import kotlinx.android.synthetic.main.fragment_machine.*
import org.koin.android.viewmodel.ext.android.viewModel

class MachineFragment : Fragment() {

    private val viewModel: MachineViewModel by viewModel()
    private lateinit var productAdapter: ProductItemAdapter

    private val onProductItemClick: (selectedItem: Stock, index: Int) -> Unit =
        { selectedItem, index ->
            if (!viewModel.isLoad.value!! && selectedItem.amount > 0) {
                val amount = selectedItem.amount - 1
                viewModel.changeStock(
                    selectedItem.id,
                    viewModel.machine.value?.id!!,
                    selectedItem.product.id,
                    selectedItem.amount - 1
                )
            }
        }

    companion object {
        @JvmStatic
        fun newInstance() = MachineFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_machine, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getMachine(1)
    }

    private fun initView() {
        productAdapter = ProductItemAdapter(onProductItemClick)
        rectProduct.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = productAdapter
        }
    }

    private fun initViewModel() {
        viewModel.machine.observe(activity!!, Observer {
            machineName.text = it.name
        })

        viewModel.productList.observe(activity!!, Observer {
            productAdapter.setProduct(it)
        })

        viewModel.isLoad.observe(activity!!, Observer {
            if (it) {
                progress.visibility = View.VISIBLE
            } else {
                progress.visibility = View.GONE
            }
        })

        viewModel.errorMessage.observe(activity!!, Observer {

        })
    }
}