package com.art.scgtest.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.art.scgtest.UseCaseResult
import com.art.scgtest.model.data.MachineModel
import com.art.scgtest.model.data.Product
import com.art.scgtest.model.data.Stock
import com.art.scgtest.model.repo.MachineRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MachineViewModel(val repo: MachineRepo) : ViewModel(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    var machine = MutableLiveData<MachineModel>()
    var productList = MutableLiveData<List<Stock>>()
    var errorMessage = MutableLiveData<String>()
    var isLoad = MutableLiveData<Boolean>()

    fun getMachine(machineId: Int) {
        Log.e("Debug", "get machine")
        launch {
            isLoad.value = true
            val result = repo.getMachine(machineId)
            when (result) {
                is UseCaseResult.Success -> {
                    machine.value = result.data
                    productList.value = result.data.stock
                }
                is UseCaseResult.Error -> errorMessage.value = result.exception.message
            }
            isLoad.value = false
        }
    }

    fun changeStock(stockId: Int, machineId: Int, productId: Int, amount: Int) {
        launch {
            isLoad.value = true
            val result = repo.changeStock(stockId, machineId, productId, amount)
            when (result) {
                is UseCaseResult.Success -> {
                    machine.value?.stock?.map {
                        Log.e("test", "$it")
                        if (it.id == result.data.id) {
                            it.amount = result.data.amount
                        }
                    }
                    productList.value = machine.value?.stock
                }
                is UseCaseResult.Error -> errorMessage.value = result.exception.message
            }
            isLoad.value = false
        }
    }
}