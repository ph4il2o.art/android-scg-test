package com.art.scgtest.model.repo

import android.util.Log
import com.art.scgtest.UseCaseResult
import com.art.scgtest.model.data.Amount
import com.art.scgtest.model.data.AmountResponse
import com.art.scgtest.model.data.MachineModel
import com.art.scgtest.model.data.MachineResponse
import com.art.scgtest.service.Api
import java.lang.Exception

interface MachineRepo {
    suspend fun getMachine(machineId: Int): UseCaseResult<MachineModel>
    suspend fun changeStock(
        stockId: Int,
        machineId: Int,
        productId: Int,
        amount: Int
    ): UseCaseResult<Amount>
}


class MachineRepoIml(private val service: Api) : MachineRepo {

    override suspend fun getMachine(machineId: Int): UseCaseResult<MachineModel> {
        return try {
            val response = service.getMachine(machineId).await()
            UseCaseResult.Success(response)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }

    override suspend fun changeStock(stockId: Int, machineId: Int, productId: Int, amount: Int): UseCaseResult<Amount> {
        return try {
            val response = service.changeStock(stockId, machineId, productId, amount).await()
            UseCaseResult.Success(response)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }

}