package com.art.scgtest.model.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Stock(
    @SerializedName("id")
    val id: Int,
    @SerializedName("product")
    val product: Product,
    @SerializedName("amount")
    var amount: Int
) : Serializable