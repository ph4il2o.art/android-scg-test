package com.art.scgtest.model.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MachineResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: Int,
    @SerializedName("previous")
    val previous: Int,
    @SerializedName("results")
    val data: MachineModel
) : Serializable