package com.art.scgtest.model.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Amount(
    @SerializedName("id")
    val id: Int,
    @SerializedName("amount")
    val amount: Int
) : Serializable