package com.art.scgtest.service

import com.art.scgtest.model.data.Amount
import com.art.scgtest.model.data.AmountResponse
import com.art.scgtest.model.data.MachineModel
import com.art.scgtest.model.data.MachineResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface Api {
    @GET("api/getMachine/{machineId}/")
    fun getMachine(@Path("machineId") id: Int): Deferred<MachineModel>

    @PUT("api/stock/{stockId}/")
    @FormUrlEncoded
    fun changeStock(
        @Path("stockId") stockId: Int,
        @Field("machine") machineId: Int,
        @Field("product") productId: Int,
        @Field("amount") amount: Int
    ): Deferred<Amount>
}