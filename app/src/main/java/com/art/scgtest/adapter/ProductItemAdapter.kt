package com.art.scgtest.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.art.scgtest.R
import com.art.scgtest.model.data.Stock
import com.bumptech.glide.Glide
import kotlin.properties.Delegates
import kotlinx.android.synthetic.main.product_item.view.*


class ProductItemAdapter(
    private val onItemClickListener:
        (selectedItem: Stock, selectedIndex: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var productList: List<Stock> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val view = inflate.inflate(R.layout.product_item, parent, false)
        return ProductHolder(view, parent.context, onItemClickListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = (holder as ProductHolder)
        viewHolder.bind(productList[position], position)
    }

    override fun getItemCount(): Int {
        Log.e("adapter", "${productList.size}")
        return productList.size
    }

    fun setProduct(newProduct: List<Stock>) {
        Log.e("adapter", "$newProduct")
        productList = newProduct
    }

    class ProductHolder(
        private val itemView: View,
        private val context: Context,
        private val onItemClickListener: (selectedItem: Stock, selectedIndex: Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(stock: Stock, position: Int) {
            itemView.productName.text = stock.product.name
            itemView.productAmount.text = stock.amount.toString()
            if (stock.amount < 1) {
                itemView.productBg.setBackgroundColor(ContextCompat.getColor(context, R.color.bright_red))
            } else {
                itemView.productBg.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            }
            Glide
                .with(context)
                .load("http://10.0.2.2:8000${stock.product.image}")
                .into(itemView.productImage)
            itemView.setOnClickListener {
                onItemClickListener(stock, position)
            }
        }
    }
}